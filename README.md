# ics-ans-lab-ioc-deploy

Ansible playbook to deploy IOC in the lab.

#### New playbook developed
Removed 03/2021:

The standard configure will add a PREROUTING to the iptables. This will push any packets on the CA port to broadcast. When multiple IOCs are running on different ports at the same time this needs to be disabled. This playbook will flush all nat tables at every boot:
  playbook_flush_networking.yml
## License

BSD 2-clause
